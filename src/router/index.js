import { createRouter, createWebHashHistory } from 'vue-router'

import Home from '../components/Home.vue'
import Store from '../components/Store.vue'

const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        { path: '/', redirect: '/home' },
        { path: '/home', component:  Home },
        { path: '/store/:store_id', component: Store, props: true }
    ]
})

export default router