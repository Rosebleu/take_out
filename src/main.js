import { createApp } from 'vue'
import App from './App.vue'
import './index.css'
import './assets/css/flexible.css'
import './assets/js/flexible.js'
// 导入路由模块
import router from './router/index'

// 导入 axios
import axios from 'axios'

const app = createApp(App)

// 使用路由
app.use(router)

// 设置 axios 的默认请求地址
axios.defaults.baseURL = 'https://mock.apifox.cn/m1/2835100-0-default/index'
// 将 axios 挂载到 $http 上全局使用
app.config.globalProperties.$http = axios

app.mount('#app')
